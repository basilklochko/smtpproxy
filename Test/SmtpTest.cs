﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmtpProxy.Controllers;
using SmtpProxy.Models;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Net.Mail;

namespace Tests
{
    [TestClass]
    public class SmtpTests
    {
        [TestMethod]
        public void Should_Send()
        {
            var ctrl = new HomeController();
            var email = new Email()
            {
                Body = "test body",
                Subject = "subject!",
                To = "bklochko@gmail.com"
            };

            ctrl.Send(email);
        }
    }
}

