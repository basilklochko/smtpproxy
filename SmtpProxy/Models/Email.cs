﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmtpProxy.Models
{
    public class Email
    {
        public string Subject { get; set; }
        public string To { get; set; }
        public string Body { get; set; }
    }
}