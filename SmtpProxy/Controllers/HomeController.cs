﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Net.Mail;
using SmtpProxy.Models;

namespace SmtpProxy.Controllers
{
    public class HomeController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Send(Email email)
        {
            var result = true;

            try
            {
                MailMessage message = new MailMessage(ConfigurationManager.AppSettings.Get("smtpFrom"), email.To, email.Subject, email.Body)
                {
                    IsBodyHtml = true
                };

                SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings.Get("smtpHost"), Int32.Parse(ConfigurationManager.AppSettings.Get("smtpPort")))
                {
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("smtpUser"), ConfigurationManager.AppSettings.Get("smtpPassword")),
                    EnableSsl = true
                };

                client.Send(message);
            }
            catch (Exception ex)
            {
                result = false;
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(result.ToString())
            };
        }
    }
}
